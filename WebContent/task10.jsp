<%@page import="interfaces.FeedbackManager"%>
<%@page import="interfaces.POIManager"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" import="acmdb.*" contentType="text/html" pageEncoding="UTF-8"%>
<html>
<head>
<title>Useful feedbacks</title>
<script LANGUAGE="javascript">

function check_all_fields(form_obj){
	if (form_obj.scr.value == "" || form_obj.poi.value == ""){
		alert("All the field should be filled!");
		return false;
	}
	return true;
}

</script> 
</head>
<body>

<%
String tag = request.getParameter("tag");
if (tag == null){
	//share all the POIs and users!
%>
	Enter the POI and the top n number: <BR>
	<form name="reg" method=get onsubmit="return check_all_fields(this)" action="task10.jsp">
		<input type=hidden name="tag" value="yes">
		Find top <input type=number name="scr" length=20> most useful feedbacks of POI
		<input type=text name="poi" length=20>!<br>
		<input type=submit>
	</form>
	<BR><BR>
<%
} else {
	String name = request.getParameter("poi");
	int scr = Integer.valueOf(request.getParameter("scr")), pid = POIManager.getPid(name);
	if (pid < 0) {
		%>
		<script>
			alert("No such POI!");
		</script>
		<%
	} else {
		ArrayList<String[]> ans = FeedbackManager.usefulQuery(pid, scr);
		String output = "The top " + scr + " most useful feedbacks: <br><br>";
		%><%=output%><%
		for (int i = 0; i < ans.size(); i++)
		{
			%><%=FeedbackManager.showFid(Integer.valueOf(ans.get(i)[0]))%><%
			output = "Judged by " + ans.get(i)[1] + " person(s), average score: " + ans.get(i)[2] + "<br><br><br>";
			%><%=output %><%
		}
	}
%>
<%
}
if ((int)session.getAttribute("type") == 0){
%>
<BR><a href="user.jsp"> Back </a>
<%
}
else{
%>
<BR><a href="admin.jsp"> Back </a>
<%
}
%>
</body>
