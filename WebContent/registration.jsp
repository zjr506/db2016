<%@page import="interfaces.UserManager"%>
<%@page language="java" import="acmdb.*" contentType="text/html" pageEncoding="UTF-8"%>
<html>
<head>
<title>Registration</title>
<script LANGUAGE="javascript">

function check_all_fields(form_obj){
	if (form_obj.id.value == "" || form_obj.psw.value == "" || form_obj.cpsw.value == "" || form_obj.usrtype.value == ""){
		alert("All the field should be filled!");
		return false;
	}
	if (form_obj.psw.value != form_obj.cpsw.value){
		alert("The passwords are not equal!");
		return false;
	}
	return true;
}

</script> 
</head>
<body>

<%
String tag = request.getParameter("tag");
if (tag == null){
%>

	Registration:<BR>
	<form name="reg" method=get onsubmit="return check_all_fields(this)" action="registration.jsp">
		<input type=hidden name="tag" value="yes">
		ID: <input type=text name="id" length=20><BR>
		Password: <input type=password name="psw" length=20><BR>
		Confirm password: <input type=password name="cpsw" length=20><BR>
		User(0) or Admin(1): <input type=number name="usrtype" length=20><BR>
		<input type=submit>
	</form>
	<BR><BR>
<%
} else {
	String id = request.getParameter("id"), password = request.getParameter("psw");
	int usrtype = Integer.valueOf(request.getParameter("usrtype"));
	int ret = UserManager.registerUser(id, password, usrtype);
	if (ret == 0) {%>
		<script>
		if (!alert("Failed, the username has been taken!"))
			location.href = 'index.jsp';
		</script>
	<%} else {%>
		<script>
		if (!alert("Registration successful!"))
			location.href = 'index.jsp';
		</script>
	<%}
	
	

}
%>

<BR><a href="index.jsp"> Back </a>

</body>
