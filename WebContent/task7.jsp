<%@page import="interfaces.FeedbackManager"%>
<%@page import="interfaces.POIManager"%>
<%@ page language="java" import="acmdb.*" contentType="text/html" pageEncoding="UTF-8"%>
<html>
<head>
<title>Usefulness ratings</title>
<script LANGUAGE="javascript">

function check_all_fields(form_obj){
	if (form_obj.poi.value == "" || form_obj.uid.value == "" || form_obj.scr.value == ""){
		alert("All the field should be filled!");
		return false;
	}
	return true;
}

</script> 
</head>
<body>

<%
String tag = request.getParameter("tag");
if (tag == null){
	//share all the POIs and users!
%>
	Choose the POI and the user: <BR>
	<form name="reg" method=get onsubmit="return check_all_fields(this)" action="task7.jsp">
		<input type=hidden name="tag" value="yes">
		POI Name: <input type=text name="poi" length=20><br>
		User Name: <input type=text name="uid" length=20><br>
		Score: <input type=number name="scr" length=20><br>
		<input type=submit>
	</form>
	<BR><BR>
<%
} else {

	String name = request.getParameter("poi"), usr = request.getParameter("uid"); 
	int scr = Integer.valueOf(request.getParameter("scr"));
	int pid = POIManager.getPid(name);
	String login = (String)session.getAttribute("nowuser");
	if (pid < 0) {
		%>
		<script>
			alert("No such POI!");
		</script>
		<%
	} else {
		int fid = FeedbackManager.getFid(usr, pid);
		if (fid < 0) {
			%>
			<script>
				alert("No such Feedback!");
			</script>
			<%
		} else {
			%><%=FeedbackManager.showFid(fid)%><%
			int ret = FeedbackManager.addUsefulness(login, fid, scr);
			if (ret == 0) {
				%>
				<script>
					alert("No such Feedback!");
				</script>
				<%
			} else if (ret == 1) {
				%>
				<script>
					alert("Don't rate your own feedback!");
				</script>
				<%
			} else if (ret == 2) {
				%>
				<script>
					alert("You have rated this feedback!");
				</script>
				<%
			} else {
				%>
				<script>
					alert("Reted successful!");
				</script>
				<%
			}
		}
	}
}
if ((int)session.getAttribute("type") == 0){
%>
<BR><a href="user.jsp"> Back </a>
<%
}
else{
%>
<BR><a href="admin.jsp"> Back </a>
<%
}
%>
</body>
