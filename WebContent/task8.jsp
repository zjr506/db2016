<%@page import="interfaces.FeedbackManager"%>
<%@ page language="java" import="acmdb.*" contentType="text/html" pageEncoding="UTF-8"%>
<html>
<head>
<title>Trust recordings</title>
<script LANGUAGE="javascript">

function check_all_fields(form_obj){
	if (form_obj.uid.value == "" || form_obj.scr.value == ""){
		alert("All the field should be filled!");
		return false;
	}
	return true;
}

</script> 
</head>
<body>

<%
String tag = request.getParameter("tag");
if (tag == null){
	//share all the users!
%>
	Choose the user: <BR>
	<form name="reg" method=get onsubmit="return check_all_fields(this)" action="task8.jsp">
		<input type=hidden name="tag" value="yes">
		User Name: <input type=text name="uid" length=20><br>
		Trust tag(0: trusted, 1: not-trusted): <input type=number name="scr" length=20><br>
		<input type=submit>
	</form>
	<BR><BR>
<%
} else {

	String usr = request.getParameter("uid");
	String login = (String)session.getAttribute("nowuser");
	int scr = Integer.valueOf(request.getParameter("scr"));
	if (scr == 0 || scr == 1) {
		int ret = FeedbackManager.addTrust(login, usr, scr);
		if (ret == 1) {
			%>
			<script>
				alert("Successful!");
			</script>
			<%
		} else if (ret == 0) {
			%>
			<script>
				alert("You have already trusted!");
			</script>
			<%
		} else {
			%>
			<script>
				alert("You can not trust yourself!");
			</script>
			<%
		}
	} else {
		%>
		<script>
			alert("You can only type 0 or 1!");
		</script>
		<%
	}
}
if ((int)session.getAttribute("type") == 0){
%>
<BR><a href="user.jsp"> Back </a>
<%
}
else{
%>
<BR><a href="admin.jsp"> Back </a>
<%
}
%>
</body>
