package interfaces;

import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

import jdk.internal.org.objectweb.asm.tree.IntInsnNode;

public class SQLExecutor {
	
	public static class Connector {
		public Connection con;
		public Statement stmt;
		public Connector() throws Exception {
			try{
			 	String userName = "acmdbu16";
		   		String password = "um56jm7s";
		        String url = "jdbc:mysql://georgia.eng.utah.edu";
			    Class.forName ("com.mysql.jdbc.Driver").newInstance ();
	        	con = DriverManager.getConnection (url, userName, password);
				stmt = con.createStatement();
	        } catch(Exception e) {
				System.err.println("Unable to open mysql jdbc connection. The error is as follows,\n");
	            System.err.println(e.getMessage());
				throw(e);
			}
		}
		
		public void closeConnection() throws Exception{
			con.close();
			stmt.close();
		}
	}

	public SQLExecutor() {
		// TODO Auto-generated constructor stub
	}
	public static ArrayList<String[]> executeQuery(String sql) throws Exception {
        Connector connector = new Connector();
        ResultSet resultSet;
        try {
        	System.err.println("Processing query:" + sql);
        	resultSet = connector.stmt.executeQuery(sql);
        } catch (Exception e) {
        	System.err.println("Execute query failed!");
        	throw(e);
        }
        
        ArrayList<String[]> resultArrayList = new ArrayList<String[]>();
        int numOfColumn = resultSet.getMetaData().getColumnCount();
        while(resultSet.next()) {
        	String[] curRow = new String[numOfColumn];
        	for(int indexOfColumn = 0; indexOfColumn < numOfColumn; ++ indexOfColumn) {
        		curRow[indexOfColumn] = resultSet.getString(indexOfColumn + 1);
        	}
        	resultArrayList.add(curRow);
        }
        
        connector.closeConnection();
        return resultArrayList;
    }
	public static void executeUpdate(String sql) throws Exception {
		Connector connector = new Connector();
		try {
			System.err.println("Processing update:" + sql);
			connector.stmt.executeUpdate(sql);
		} catch (Exception e) {        	
			System.err.println("Execute update failed!");
			throw(e);
		}
		connector.closeConnection();
	}
	public static void runScript(InputStream in) throws Exception {
		Connector connector = new Connector();
		Scanner s = new Scanner(in);
		s.useDelimiter("/\\*[\\s\\S]*?\\*/|--[^\\r\\n]*|;");
		Statement st = connector.stmt;

		try{
			while (s.hasNext()) {
				String line = s.next().trim();
				System.err.println("Processing script: " + line);
		        if (!line.isEmpty()) st.execute(line);
		    }
		} finally {
			s.close();
			connector.closeConnection();
		}
	}
}
