<%@page import="interfaces.POIManager"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" import="acmdb.*" contentType="text/html" pageEncoding="UTF-8"%>
<html>
<head>
<title>Statistics</title>
<script LANGUAGE="javascript">

function check_all_fields(form_obj){
	if (form_obj.m.value == "" || form_obj.type.value == ""){
		alert("All the field should be filled!");
		return false;
	}
	return true;
}

</script> 
</head>
<body>

<%
String tag = request.getParameter("tag");
if (tag == null){
	//share all the users!
%>
	Input the top x POIs: <BR>
	<form name="reg" method=get onsubmit="return check_all_fields(this)" action="task13.jsp">
		<input type=hidden name="tag" value="yes">
		x(1-10): <input type=number name="m" length=20><br>
		most popular(0), most expensive(1), highest rated(2): <input type=number name="type" length=20><br>
		<input type=submit>
	</form>
	<BR><BR>
<%
} else {

	int top = Integer.valueOf(request.getParameter("m"));
	int type = Integer.valueOf(request.getParameter("type"));
	if (type == 0) {
		out.println("Top " + top + " popular POIs for each category:<br>");
		ArrayList<String[]> result = POIManager.getPopularList(top);
		for(int i = 0; i < result.size(); ++ i) {
			out.print(result.get(i)[0] + ":");
			for(int j = 1; j < result.get(i).length; ++ j) {
				out.print(" " + result.get(i)[j]);
			}			
			out.println("<br>");
		}
	}
	if (type == 1) {
		out.println("Top " + top + " expensive POIs for each category:<br>");
		ArrayList<String[]> result = POIManager.getExpensiveList(top);
		for(int i = 0; i < result.size(); ++ i) {
			out.print(result.get(i)[0] + ":");
			for(int j = 1; j < result.get(i).length; ++ j) {
				out.print(" " + result.get(i)[j]);
			}			
			out.println("<br>");
		}
	}
	if (type == 2) {
		out.println("Top " + top + " high rated POIs for each category:<br>");
		ArrayList<String[]> result = POIManager.getHighRatedList(top);
		for(int i = 0; i < result.size(); ++ i) {
			out.print(result.get(i)[0] + ":");
			for(int j = 1; j < result.get(i).length; ++ j) {
				out.print(" " + result.get(i)[j]);
			}			
			out.println("<br>");
		}
	}
}
if ((int)session.getAttribute("type") == 0){
%>
<BR><a href="user.jsp"> Back </a>
<%
}
else{
%>
<BR><a href="admin.jsp"> Back </a>
<%
}
%>
</body>
