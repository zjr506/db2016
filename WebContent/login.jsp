<%@page import="interfaces.UserManager"%>
<%@ page language="java" import="acmdb.*" contentType="text/html" pageEncoding="UTF-8"%>
<html>
<head>
<title>Login</title>
<script LANGUAGE="javascript">

function check_all_fields(form_obj){
	if (form_obj.id.value == "" || form_obj.psw.value == ""){
		alert("All the field should be filled!");
		return false;
	}
	return true;
}

</script> 
</head>
<body>

<%
String tag = request.getParameter("tag");
if (tag == null){
%>

	Login:<BR>
	<form name="reg" method=get onsubmit="return check_all_fields(this)" action="login.jsp">
		<input type=hidden name="tag" value="yes">
		ID: <input type=text name="id" length=20><BR>
		Password: <input type=password name="psw" length=20><BR>
		<input type=submit>
	</form>
	<BR><BR>
<%
} else {

	String id = request.getParameter("id"), password = request.getParameter("psw");
	int ret = UserManager.loginUser(id, password);
	if (ret < 0) {%>
		<script>
		if (!alert("Login failed!"))
			location.href = 'index.jsp';
		</script><%
	}
	session.setAttribute("nowuser", id);
	session.setAttribute("type", ret);
	if (ret == 0) {%>
		<script>
			if (!alert("Login successfully."))
				location.href = 'user.jsp';
		</script><%
	} else {%>
		<script>
			if (!alert("Login successfully."))
				location.href = 'admin.jsp';
		</script><%
	}
}
%>

<BR><a href="index.jsp"> Back </a>

</body>
