<%@page import="interfaces.POIManager"%>
<%@ page language="java" import="acmdb.*" contentType="text/html" pageEncoding="UTF-8"%>
<html>
<head>
<title>POI Browsing</title>
<script LANGUAGE="javascript">

function check_all_fields(form_obj){
	if (form_obj.p1.value == "" || form_obj.p2.value == ""){
		alert("All the field of prices should be filled! (You can made a large scale if you don't want, but necessary)");
		return false;
	}
	return true;
}

</script> 
</head>
<body>

<%
String tag = request.getParameter("tag");
if (tag == null){
%>
	Enter your new POI:<BR>
	<form name="reg" method=get onsubmit="return check_all_fields(this)" action="task9.jsp">
		<input type=hidden name="tag" value="yes">
		Price lower bound: <input type=number name="p1" length=20><br>
		Price upper bound: <input type=number name="p2" length=20><br>
		City (Optional): <input type=text name="city" length=50><br>
		State (Optional): <input type=text name="state" length=50><br>
		Keywords (Optional): <input type="text" name="key" length=50><br>
		Category (Optional): <input type="text" name="cat" length=20><br>
		Conjunction: <input type="radio" name="conj" value=" AND " checked>and
		<input type="radio" name="conj" value=" OR ">or<br>
		Sort ways: <input type="radio" name="srt" value=0 checked>by price
		<input type="radio" name="srt" value=1>by average score of the feedbacks
		<input type="radio" name="srt" value=2>by average score of the feedbacks from trusted<br>
		<input type=submit>
	</form>
	<BR><BR>
<%
} else {

	String 	city = request.getParameter("city"), 
			state = request.getParameter("state"), 
			key = request.getParameter("key"), 
			cat = request.getParameter("cat"),
			conj = request.getParameter("conj");
	double	p1 = Double.valueOf(request.getParameter("p1")),
			p2 = Double.valueOf(request.getParameter("p2"));
	int srt = Integer.valueOf(request.getParameter("srt"));
	%><%=POIManager.browse(p1, p2, city, state, key, cat, srt, conj) %><%
}
if ((int)session.getAttribute("type") == 0){
%>
<BR><a href="user.jsp"> Back </a>
<%
}
else{
%>
<BR><a href="admin.jsp"> Back </a>
<%
}
%>
</body>
