<%@page import="interfaces.FeedbackManager"%>
<%@page import="interfaces.POIManager"%>
<%@ page language="java" import="acmdb.*" contentType="text/html" pageEncoding="UTF-8"%>
<html>
<head>
<title>Visiting suggestions</title>
<script LANGUAGE="javascript">

function check_all_fields(form_obj){
	if (form_obj.pid.value == ""){
		alert("All the field should be filled!");
		return false;
	}
	return true;
}

</script> 
</head>
<body>

<%
String tag = request.getParameter("tag");
if (tag == null){
	//share all the POIs!
%>
	Check the suggestions: <BR>
	<form name="reg" method=get onsubmit="return check_all_fields(this)" action="task11.jsp">
		<input type=hidden name="tag" value="yes">
		Which POI you have visited? <input type=number name="pid" length=20><br>
		<input type=submit>
	</form>
	<BR><BR>
<%
} else {

	String name = request.getParameter("pid");
	int poi = POIManager.getPid(name);
	//
	%>
	The top visit(s):<br><%=FeedbackManager.suggest(poi) %><br>
<%
}
if ((int)session.getAttribute("type") == 0){
%>
<BR><a href="user.jsp"> Back </a>
<%
}
else{
%>
<BR><a href="admin.jsp"> Back </a>
<%
}
%>
</body>
