<%@ page language="java" import="acmdb.*" contentType="text/html" pageEncoding="UTF-8"%>
<html>
<head>
<title>Admin</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>

Hello, <%=(String)session.getAttribute("nowuser") %>!<br>

<a href="userinfo.jsp">Change information</a><br>
<a href="task2.jsp">Visit</a><br>
<a href="task3.jsp">New POI</a><br>
<a href="task4.jsp">Update POI</a><br>
<a href="task5.jsp">Favorite recordings</a><br>
<a href="task6.jsp">Feedback recordings</a><br>
<a href="task7.jsp">Usefulness ratings</a><br>
<a href="task8.jsp">Trust recordings</a><br>
<a href="task9.jsp">POI browsing</a><br>
<a href="task10.jsp">Useful feedbacks</a><br>
<a href="task11.jsp">Visiting suggestions</a><br>
<a href="task12.jsp">2 degrees of separation</a><br>
<a href="task13.jsp">Statistics</a><br>
<a href="task14.jsp">User awards</a><br>
<a href="logout.jsp">Logout</a><br>

</body>
</html>
