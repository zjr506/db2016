<%@page import="java.sql.Date"%>
<%@page import="interfaces.VisitManager"%>
<%@page import="interfaces.POIManager"%>
<%@ page language="java" import="acmdb.*" contentType="text/html" pageEncoding="UTF-8"%>
<html>
<head>
<title>Visit</title>
<script LANGUAGE="javascript">

function check_all_fields(form_obj){
	if (form_obj.poi.value == "" || form_obj.date.value == "" || form_obj.cost.value == "" ||
		form_obj.man.value == ""){
		alert("All the field should be filled!");
		return false;
	}
	return true;
}

</script> 
</head>
<body>

<%
String tag = request.getParameter("tag");
if (tag == null){
%>
	Enter your new visit:<BR>
	<form name="reg" method=get onsubmit="return check_all_fields(this)" action="task2.jsp">
		<input type=hidden name="tag" value="yes">
		POI name: <input type=text name="poi" length=20><BR>
		Date(YYYY-MM-DD): <input type=date name="date" length=20><BR>
		Cost(CNY): <input type=number name="cost" length=20><BR>
		Number of person: <input type=number name="man" length=20><BR>
		<input type=submit>
	</form>
	<BR><BR>
<%
} else {

	String poi = request.getParameter("poi");
	Date date = Date.valueOf(request.getParameter("date")); 
	int cost = Integer.valueOf(request.getParameter("cost")), 
			man = Integer.valueOf(request.getParameter("man"));
	String usr = (String)session.getAttribute("nowuser");
	int pid = POIManager.getPid(poi);
	if (pid < 0){ %>
		<script>alert("Can't find the POI!");</script>
	<%}else{
		VisitManager.insertVisit(usr, pid, cost, man, date); %>
		<script>alert("Added successfully!");</script>
		<br><a href="task2.jsp"> Continue adding </a>
	<%
	}
}
if ((int)session.getAttribute("type") == 0){
%>
<BR><a href="user.jsp"> Back </a>
<%
}
else{
%>
<BR><a href="admin.jsp"> Back </a>
<%
}
%>
</body>
