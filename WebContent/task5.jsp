<%@page import="interfaces.FeedbackManager"%>
<%@page import="interfaces.POIManager"%>
<%@ page language="java" import="acmdb.*" contentType="text/html" pageEncoding="UTF-8"%>
<html>
<head>
<title>Favorite recordings</title>
<script LANGUAGE="javascript">

function check_all_fields(form_obj){
	if (form_obj.id.value == ""){
		alert("All the field should be filled!");
		return false;
	}
	return true;
}

</script> 
</head>
<body>

<%
String tag = request.getParameter("tag");
if (tag == null){
	//first share all the POIs!
%>
	What's your favorite?<BR>
	<form name="reg" method=get onsubmit="return check_all_fields(this)" action="task5.jsp">
		<input type=hidden name="tag" value="yes">
		Name of Your favorite POI: <input type=text name="id" length=20><br>
		<input type=submit>
	</form>
	<BR><BR>
<%
} else {

	String id = request.getParameter("id");
	int pid = POIManager.getPid(id);
	if (pid < 0) { %>
		<script>
			alert("No such POI!");
		</script>
	<%}else{
		String login = (String)session.getAttribute("nowuser");
		FeedbackManager.addFavorite(login, pid);
		%><script>
			alert("Set successfully!");
		</script>
	<%
	}
}
if ((int)session.getAttribute("type") == 0){
%>
<BR><a href="user.jsp"> Back </a>
<%
}
else{
%>
<BR><a href="admin.jsp"> Back </a>
<%
}
%>
</body>
