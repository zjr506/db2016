package interfaces;

import java.util.ArrayList;
import java.util.TreeMap;

public class POIManager {

	public POIManager() {
		// TODO Auto-generated constructor stub
	}
	public static int getPid(String name) throws Exception { // -1: failed
		ArrayList<String[]> result = SQLExecutor.executeQuery("SELECT * FROM acmdb16.POIS WHERE name=\"" + name + "\";");
		if (result.size() == 0) {
			return -1;
		} else {
			return Integer.valueOf(result.get(0)[0]);
		}
	}
	public static int insertPOI(String name, String cate, String street, String city, String state, String url, String keyword, double price, String phone, long yrs, long hrs) throws Exception {
		ArrayList<String[]> result = SQLExecutor.executeQuery("SELECT * FROM acmdb16.POIS WHERE name=\"" + name + "\";");
		if (result.size() == 0) {
			SQLExecutor.executeUpdate("INSERT INTO acmdb16.POIS (name, cate, street, city, state, url, keyword, price, phone, yrs, hrs) VALUES ("
					+ "\"" + name + "\","
					+ "\"" + cate + "\","
					+ "\"" + street + "\","
					+ "\"" + city + "\","
					+ "\"" + state + "\","
					+ "\"" + url + "\","
					+ "\"" + keyword + "\","
					+ price + ","					
					+ "\"" + phone + "\","
					+ yrs + ","
					+ hrs
					+ ");");
			return 1;
		} else {
			return 0;
		}
	}
	public static int updatePOI(String name, String cate, String street, String city, String state, String url, String keyword, double price, String phone, long yrs, long hrs) throws Exception {
		ArrayList<String[]> result = SQLExecutor.executeQuery("SELECT * FROM acmdb16.POIS WHERE name=\"" + name + "\";");
		if (result.size() == 0) {
			return 0;
		} else {
			SQLExecutor.executeUpdate("UPDATE acmdb16.POIS SET "
					+ "cate= " + "\"" + cate + "\","
					+ "street= " + "\"" + street + "\","
					+ "city= " + "\"" + city + "\","
					+ "state= " + "\"" + state + "\","
					+ "url= " + "\"" + url + "\","
					+ "keyword= " + "\"" + keyword + "\","
					+ "price= " + price + ","
					+ "phone= " + "\"" + phone + "\","
					+ "yrs= " + yrs + ","
					+ "hrs= " + hrs
					+ " WHERE name=\"" + name + "\";");
			return 1;
		}
	}
	static ArrayList<String> getCategoryList() throws Exception {
		ArrayList<String> answer = new ArrayList<String>();
		ArrayList<String[]> result = SQLExecutor.executeQuery("SELECT DISTINCT(cate) As cate FROM acmdb16.POIS;");
		System.err.println("Getting category list: ");
		for(int i = 0; i < result.size(); ++ i) {
			System.err.println(result.get(i)[0] + " ");
			answer.add(result.get(i)[0]);
		}
		System.err.println();
		return answer;
	}
	static ArrayList<String[]> getPopularListOfCate(int top, String category) throws Exception {
        return SQLExecutor.executeQuery("SELECT p.name FROM acmdb16.POIS p, (SELECT pid, count(*) as cnt FROM acmdb16.VISIT v GROUP BY v.pid) AS c WHERE p.pid = c.pid AND p.cate = \"" + category + "\" ORDER BY c.cnt DESC LIMIT " + top + ";");
	}
	public static ArrayList<String[]> getPopularList(int top) throws Exception {
		ArrayList<String> cateList =  getCategoryList();
		ArrayList<String[]> answer = new ArrayList<String[]>();
		for(int i = 0; i < cateList.size(); ++ i) {
			ArrayList<String[]> topPopular = getPopularListOfCate(top, cateList.get(i));
			String[] line = new String[topPopular.size() + 1];
			line[0] = cateList.get(i);
			for(int j = 0; j < topPopular.size(); ++ j) line[j + 1] = topPopular.get(j)[0];
			answer.add(line);
		}
		return answer;
	}	
	static ArrayList<String[]> getExpensiveListOfCate(int top, String category) throws Exception {
        return SQLExecutor.executeQuery("SELECT p.name FROM acmdb16.POIS p, (SELECT vv.pid, avg(cost) AS avg FROM (SELECT v.pid, v.vid, (ve.cost / ve.numofperson) AS cost FROM acmdb16.VISIT v, acmdb16.VIS_EVENT ve WHERE v.vid = ve.vid) AS vv GROUP BY vv.pid) AS a WHERE p.pid = a.pid AND p.cate = \"" + category + "\" ORDER BY a.avg DESC LIMIT " + top + ";");
	}
	public static ArrayList<String[]> getExpensiveList(int top) throws Exception {
		ArrayList<String> cateList =  getCategoryList();
		ArrayList<String[]> answer = new ArrayList<String[]>();
		for(int i = 0; i < cateList.size(); ++ i) {
			ArrayList<String[]> topPopular = getExpensiveListOfCate(top, cateList.get(i));
			String[] line = new String[topPopular.size() + 1];
			line[0] = cateList.get(i);
			for(int j = 0; j < topPopular.size(); ++ j) line[j + 1] = topPopular.get(j)[0];
			answer.add(line);
		}
		return answer;
	}	
	static ArrayList<String[]> getHighRatedListOfCate(int top, String category) throws Exception {
        return SQLExecutor.executeQuery("SELECT p.name FROM acmdb16.POIS p, (SELECT pid, avg(score) AS avg FROM acmdb16.FEEDBACKS GROUP BY pid) AS a WHERE p.pid = a.pid AND p.cate = \"" + category + "\" ORDER BY avg DESC LIMIT " + top + ";");
	}
	public static ArrayList<String[]> getHighRatedList(int top) throws Exception {
		ArrayList<String> cateList =  getCategoryList();
		ArrayList<String[]> answer = new ArrayList<String[]>();
		for(int i = 0; i < cateList.size(); ++ i) {
			ArrayList<String[]> topPopular = getHighRatedListOfCate(top, cateList.get(i));
			String[] line = new String[topPopular.size() + 1];
			line[0] = cateList.get(i);
			for(int j = 0; j < topPopular.size(); ++ j) line[j + 1] = topPopular.get(j)[0];
			answer.add(line);
		}
		return answer;
	}
	
	public static String browse(double p1, double p2, String city, String state,
								String keyword, String cate, int srt, String conj) throws Exception{
		String ask = "SELECT * FROM acmdb16.POIS WHERE ";
		ask = ask + "price >= " + p1 + conj +"price <= " + p2;
		if (city.compareTo("") != 0)
			ask = ask + conj + "city=\"" + city + "\"";
		if (state.compareTo("") != 0)
			ask = ask + conj + "state=\"" + state + "\"";
		if (keyword.compareTo("") != 0)
			ask = ask + conj + "keyword=\"" + keyword + "\"";
		if (cate.compareTo("") != 0)
			ask = ask + conj + "cate=\"" + cate + "\"";
		ask = ask + ";";
		ArrayList<String[]> result = SQLExecutor.executeQuery(ask);
		int[] ord = new int[result.size()];
		for (int i = 0; i < result.size(); i++)
			ord[i] = i;
		String output = "";
		if (srt == 0)
		{
			double[] all = new double[result.size()];
			for (int i = 0; i < result.size(); i++)
				all[i] = Double.parseDouble(result.get(i)[8]);
			for (int i = 0; i < result.size(); i++)
				for (int j = i + 1; j < result.size(); j++)
					if (all[ord[i]] < all[ord[j]])
					{
						int tmp = ord[i]; ord[i] = ord[j]; ord[j] = tmp;
					}
			for (int i = 0; i < result.size(); i++)
				output = output + "The rank " + (i + 1) + " is POI " + result.get(ord[i])[0] + ", which named "
								+ result.get(ord[i])[1] + ", the price is " + all[ord[i]] + ".<br>";
			return output;
		}
		if (srt == 1)
		{
			double[] all = new double[result.size()];
			for (int i = 0; i < result.size(); i++)
			{
				ArrayList<String[]> result1 = SQLExecutor.executeQuery("SELECT * FROM acmdb16.FEEDBACKS WHERE pid=" + result.get(i)[0] + ";");
				all[i] = 0;
				for (int j = 0; j < result1.size(); j++)
					all[i] += Integer.valueOf(result1.get(j)[4]);
				if (result1.size() != 0)
					all[i] /= result1.size();
			}
			for (int i = 0; i < result.size(); i++)
				for (int j = i + 1; j < result.size(); j++)
					if (all[ord[i]] < all[ord[j]])
					{
						int tmp = ord[i]; ord[i] = ord[j]; ord[j] = tmp;
					}
			for (int i = 0; i < result.size(); i++)
				output = output + "The rank " + (i + 1) + " is POI " + result.get(ord[i])[0] + ", which named "
								+ result.get(ord[i])[1] + ", the average score of the feedbacks is " + all[ord[i]] + ".<br>";
			return output;
		}
		else
		{
			int[] trust = new int[1000];
			ArrayList<String[]> alltrust = SQLExecutor.executeQuery("SELECT * FROM acmdb16.TRUSTS");
			TreeMap<String, Integer> hash = new TreeMap<String, Integer>();
			int[] rev = new int[1000];
			int sump = 0;
			for (int i = 0; i < 1000; i++)
				trust[i] = 0;
			for (int i = 0; i < alltrust.size(); i++)
			{
				if (!hash.containsKey(alltrust.get(i)[1]))
				{
					hash.put(alltrust.get(i)[1], sump);
					rev[sump] = i;
					sump++;
				}
				int uid = hash.get(alltrust.get(i)[1]);
				if (Integer.valueOf(alltrust.get(i)[2]) == 0)
					trust[uid]++;
				else
					trust[uid]--;
			}
			double[] all = new double[result.size()];
			for (int i = 0; i < result.size(); i++)
			{
				ArrayList<String[]> result1 = SQLExecutor.executeQuery("SELECT * FROM acmdb16.FEEDBACKS WHERE pid=" + result.get(i)[0] + ";");
				all[i] = 0;
				int used = 0;
				for (int j = 0; j < result1.size(); j++)
					if (!hash.containsKey(result1.get(j)[1]) || trust[hash.get(result1.get(j)[1])] >= 0)
					{
						all[i] += Integer.valueOf(result1.get(j)[4]);
						used++;
					}
				if (used > 0)
					all[i] /= used;
			}
			for (int i = 0; i < result.size(); i++)
				for (int j = i + 1; j < result.size(); j++)
					if (all[ord[i]] < all[ord[j]])
					{
						int tmp = ord[i]; ord[i] = ord[j]; ord[j] = tmp;
					}
			for (int i = 0; i < result.size(); i++)
				output = output + "The rank " + (i + 1) + " is POI " + result.get(ord[i])[0] + ", which named "
								+ result.get(ord[i])[1] + ", the average score of the feedbacks from trusted is " + all[ord[i]] + ".<br>";
			return output;
		}
	}
}
