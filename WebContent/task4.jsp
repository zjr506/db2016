<%@page import="interfaces.POIManager"%>
<%@ page language="java" import="acmdb.*" contentType="text/html" pageEncoding="UTF-8"%>
<html>
<head>
<title>Update POI</title>
<script LANGUAGE="javascript">

function check_all_fields(form_obj){
	if (form_obj.id.value == "" || form_obj.poi.value == "" || form_obj.street.value == "" || form_obj.city.value == "" || form_obj.state.value == "" ||
		form_obj.url.value == "" || form_obj.phone.value == "" || form_obj.yrs.value == "" ||
		form_obj.hrs.value == "" || form_obj.val.value == "" || form_obj.key.value == "" ||
		form_obj.cat.value == ""){
		alert("All the field should be filled!");
		return false;
	}
	return true;
}

</script> 
</head>
<body>

<%
String tag = request.getParameter("tag");
if (tag == null){
%>
	Enter the update POI information:<BR>
	<form name="reg" method=get onsubmit="return check_all_fields(this)" action="task4.jsp">
		<input type=hidden name="tag" value="yes">
		POI name: <input type=text name="name" length=20><BR>
		Street: <input type=text name="street" length=50><br>
		City: <input type=text name="city" length=50><br>
		State: <input type=text name="state" length=50><br>
		URL: <input type=text name="url" length=50><br>
		Phone: <input type=text name="phone" length=20><br>
		Year of establishment: <input type=number name="yrs" length=20><br>
		Hours of operation: <input type=text name="hrs" length=20><br>
		Price as estimated expected price per person per visit (CNY): <input type=number name="val" length=20><br>
		Keywords: <input type="text" name="key" length=50><br>
		Category: <input type="text" name="cat" length=20><br>
		<input type=submit>
	</form>
	<BR><BR>
<%
} else {

	String poi = request.getParameter("name"), 
			street = request.getParameter("street"), 
			city = request.getParameter("city"), 
			state = request.getParameter("state"), 
			url = request.getParameter("url"),
			key = request.getParameter("key"), 
			cat = request.getParameter("cat"),
			phone = request.getParameter("phone"); 
	long yrs = Long.valueOf(request.getParameter("yrs")),
		hrs = Long.valueOf(request.getParameter("hrs"));
	double val = Double.valueOf(request.getParameter("val"));
	int pid = POIManager.getPid(poi);
	if (pid < 0) {%>
		<script>
			if (!alert("No such POI name!"))
				location.href = 'admin.jsp';
		</script>
	<%}else{
		POIManager.updatePOI(poi, cat, street, city, state, url, key, val, phone, yrs, hrs);%>
		<script>
			if (!alert("Change successfully!"))
				location.href = 'admin.jsp';
		</script> <%
	}
}
if ((int)session.getAttribute("type") == 0){
%>
<BR><a href="user.jsp"> Back </a>
<%
}
else{
%>
<BR><a href="admin.jsp"> Back </a>
<%
}
%>
</body>
