package interfaces;

import java.sql.Date;
import java.util.ArrayList;
import java.util.TreeMap;

public class FeedbackManager {

	public FeedbackManager() {
		// TODO Auto-generated constructor stub
	}
	public static int getFid(String login, int pid) throws Exception {
		ArrayList<String[]> result = SQLExecutor.executeQuery("SELECT * FROM acmdb16.FEEDBACKS WHERE login=\""+login+"\" AND pid=" + pid + ";");
		if (result.size() > 0) {
			return Integer.valueOf(result.get(0)[0]);
		} else {
			return -1;
		}
	}
	public static int addFeekback(String login, int pid, Date fdate, int score, String comment) throws Exception {
		ArrayList<String[]> result = SQLExecutor.executeQuery("SELECT * FROM acmdb16.FEEDBACKS WHERE login=\""+login+"\" AND pid=" + pid + ";");
		if (result.size() > 0) {
			return 0;
		} else {
			SQLExecutor.executeUpdate("INSERT INTO acmdb16.FEEDBACKS (login, pid, fdate, score, comment) VALUES ("
					+ "\"" + login + "\","
					+ pid + ","
					+ "\"" + fdate.toString() + "\","
					+ score + ","
					+ "\"" + comment + "\""
					+ ");");
			return 1;
		}
	}
	
	public static int addUsefulness(String login, int fid, int rating) throws Exception { 
		//0:no such fid, 1:rate my own feedback, 2:have added, 3:success
		ArrayList<String[]> result = SQLExecutor.executeQuery("SELECT * FROM acmdb16.FEEDBACKS WHERE fid="+fid + ";");
		if (result.size() > 0) {
			if (result.get(0)[1].compareTo(login) == 0) {
				return 1;
			}
			ArrayList<String[]> result2 = SQLExecutor.executeQuery("SELECT * FROM acmdb16.USEFULNESS WHERE fid="+fid + " AND login=\"" + login + "\";");
			if (result2.size() > 0) {
				return 2;
			}
			SQLExecutor.executeUpdate("INSERT INTO acmdb16.USEFULNESS (login, fid, rating, login_y, pid) VALUES ("
					+ "\"" + login + "\","
					+ fid + ","
					+ rating + ","
					+ "\"" + result.get(0)[1] + "\"" + ","
					+ result.get(0)[2]
					+ ");");
			return 3;
		} else {
			return 0;
		}

	}
	
	public static int addFavorite(String login, int pid) throws Exception {
		ArrayList<String[]> result = SQLExecutor.executeQuery("SELECT * FROM acmdb16.FAVORITES WHERE login=\""+login+"\" AND pid=" + pid + ";");
		if (result.size() > 0) {
			return 0;
		} else {
			SQLExecutor.executeUpdate("INSERT INTO acmdb16.FAVORITES (login, pid) VALUES ("
					+ "\"" + login + "\","
					+ pid + ");");
			return 1;
		}
	}
	
	public static int addTrust(String login_x, String login_y, int tag) throws Exception {
		//0:have trusted, 1:success, 2:login_x == login_y
		if (login_x.compareTo(login_y) == 0) {
			return 2;
		}
		ArrayList<String[]> result = SQLExecutor.executeQuery("SELECT * FROM acmdb16.TRUSTS WHERE login_x=\""+login_x+"\" AND login_y=\"" + login_y + "\";");
		if (result.size() > 0) {
			return 0;
		} else {
			SQLExecutor.executeUpdate("INSERT INTO acmdb16.TRUSTS (login_x, login_y, tag) VALUES ("
					+ "\"" + login_x + "\","
					+ "\"" + login_y + "\","
					+ tag
					+ ");");
			return 1;
		}
	}
	
	public static int eraseTrust(String login_x, String login_y) throws Exception {
		ArrayList<String[]> result = SQLExecutor.executeQuery("SELECT * FROM acmdb16.TRUSTS WHERE login_x=\""+login_x+"\" AND login_y=\"" + login_y + "\";");
		if (result.size() == 0) {
			return 0;
		} else {
			SQLExecutor.executeUpdate("DELETE FROM acmdb16.TRUSTS WHERE login_x=\""+login_x+"\" AND login_y=\"" + login_y + "\";");
			return 1;
		}
	}
	
	public static String showFid(int fid) throws Exception {
		ArrayList<String[]> result = SQLExecutor.executeQuery("SELECT * FROM acmdb16.FEEDBACKS WHERE fid="+fid + ";");
		String output = "By user " + result.get(0)[1] +", feedback to " + result.get(0)[2] + ".<br>" +
				"Date: " + result.get(0)[3] + ", Score: " + result.get(0)[4] + "<br>" +
				"Comment: " + result.get(0)[5] + "<br><br>";
		return output;
	}
	
	public static ArrayList<String[]> usefulQuery(int pid, int tops) throws Exception {
		ArrayList<String[]> result = SQLExecutor.executeQuery("SELECT * FROM acmdb16.USEFULNESS WHERE pid="+pid + ";");
		if (result.size() < tops)
			tops = result.size();
		int maxfid = 0;
		for (int i = 0; i < result.size(); i++)
		{
			int tmp = Integer.valueOf(result.get(i)[1]);
			if (tmp > maxfid)
				maxfid = tmp;
		}
		maxfid++;
		int[] vis = new int[maxfid];
		int[] sum = new int[maxfid];
		for (int i = 0; i < maxfid; i++)
			vis[i] = sum[i] = 0;
		for (int i = 0; i < result.size(); i++)
		{
			vis[Integer.valueOf(result.get(i)[1])]++;
			sum[Integer.valueOf(result.get(i)[1])] += Integer.valueOf(result.get(i)[2]);
		}
        ArrayList<String[]> resultArrayList = new ArrayList<String[]>();
		for (int i = 0; i < tops; i++)
		{
			int k = -1;
			for (int j = 0; j < maxfid; j++)
			{
				if (vis[j] == 0)
					continue;
				if (k == -1 || sum[j] * vis[k] > sum[k] * vis[j])
					k = j;
			}
			if (k == -1)
				break;
			String[] nowans = new String[3];
			nowans[0] = "" + k;
			nowans[1] = "" + vis[k];
			nowans[2] = "" + (1.0 * sum[k] / vis[k]);
			resultArrayList.add(nowans);
			vis[k] = 0;
		}
		return resultArrayList;
	}
	
	public static String award(int tops) throws Exception {
		String output = "";
		ArrayList<String[]> result = SQLExecutor.executeQuery("SELECT * FROM acmdb16.TRUSTS;");
		TreeMap<String, Integer> hash = new TreeMap<String, Integer>();
		int[] rev = new int[1000];
		hash.clear();
		int[] vis = new int[1000];
		for (int i = 0; i < 1000; i++)
			vis[i] = 0;
		int sump = 0;
		for (int i = 0; i < result.size(); i++)
		{
			if (!hash.containsKey(result.get(i)[1]))
			{
				hash.put(result.get(i)[1], sump);
				rev[sump] = i;
				sump++;
			}
			int uid = hash.get(result.get(i)[1]);
			if (Integer.valueOf(result.get(i)[2]) == 0)
				vis[uid]++;
			else
				vis[uid]--;
		}
		for (int i = 1; i <= tops; i++)
		{
			int k = -1;
			for (int j = 0; j < sump; j++)
				if (vis[j] > -1000 && (k == -1 || vis[k] < vis[j]))
					k = j;
			if (k == -1)
				break;
			output = output + "The rank " + i + " of trusted is " + result.get(rev[k])[1] + "!<br>";
			vis[k] = -1000;
		}
		output = output + "------------------------------------<br>";
		
		result = SQLExecutor.executeQuery("SELECT * FROM acmdb16.USEFULNESS;");
		int[] sum = new int[1000];
		for (int i = 0; i < 1000; i++)
			vis[i] = sum[i] = 0;
		hash.clear();
		sump = 0;
		for (int i = 0; i < result.size(); i++)
		{
			if (!hash.containsKey(result.get(i)[3]))
			{
				hash.put(result.get(i)[3], sump);
				rev[sump] = i;
				sump++;
			}
			int uid = hash.get(result.get(i)[3]);
			vis[uid]++;
			sum[uid] += Integer.valueOf(result.get(i)[2]);
		}
		for (int i = 1; i <= tops; i++)
		{
			int k = -1;
			for (int j = 0; j < sump; j++)
				if (vis[j] > 0 && (k == -1 || sum[j] * vis[k] > sum[k] * vis[j]))
					k = j;
			if (k == -1)
				break;
			output = output + "The rank " + i + " of useful is " + result.get(rev[k])[3] + "!<br>";
			vis[k] = -1000;
		}
		return output;
	}
	public static String degree(String u1, String u2) throws Exception {
		if (u1.compareTo(u2) == 0)
			return "0";
		ArrayList<String[]> result = SQLExecutor.executeQuery("SELECT * FROM acmdb16.FAVORITES WHERE login=\"" + u1 + "\";");
		int[] vis = new int[1000];
		for (int i = 0; i < 1000; i++)
			vis[i] = 0;
		for (int i = 0; i < result.size(); i++)
			vis[Integer.valueOf(result.get(i)[1])] = 1;
		ArrayList<String[]> result1 = SQLExecutor.executeQuery("SELECT * FROM acmdb16.FAVORITES WHERE login=\"" + u2 + "\";");
		for (int i = 0; i < result1.size(); i++)
		{
			if (vis[Integer.valueOf(result1.get(i)[1])] != 0)
				return "1";
			vis[Integer.valueOf(result1.get(i)[1])] = 2;
		}
		for (int i = 0; i < 1000; i++)
		{
			if (vis[i] != 1)
				continue;
			ArrayList<String[]> result2 = SQLExecutor.executeQuery("SELECT * FROM acmdb16.FAVORITES WHERE pid=" + i + ";");
			for (int j = 0; j < result2.size(); j++)
			{
				if (result2.get(j)[0].compareTo(u1) == 0)
					continue;
				ArrayList<String[]> result3 = SQLExecutor.executeQuery("SELECT * FROM acmdb16.FAVORITES WHERE login=\"" + result2.get(j)[0] + "\";");
				for (int k = 0; k < result3.size(); k++)
					if (vis[Integer.valueOf(result3.get(j)[1])] == 2)
						return "2";
			}
		}
		return "Bigger than 2";
	}

	public static String suggest(int pid) throws Exception {
		ArrayList<String[]> result = SQLExecutor.executeQuery("SELECT * FROM acmdb16.VISIT WHERE pid=" + pid + ";");
		int[] vis = new int[1000];
		for (int i = 0; i < 1000; i++)
			vis[i] = 0;
		TreeMap<String, Integer> hash = new TreeMap<String, Integer>();
		for (int i = 0; i < result.size(); i++)
		{
			if (!hash.containsKey(result.get(i)[0]))
				hash.put(result.get(i)[0], 1);
			else
				continue;
			ArrayList<String[]> result1 = SQLExecutor.executeQuery("SELECT * FROM acmdb16.VISIT WHERE login=\"" + result.get(i)[0] + "\";");
			for (int j = 0; j < result1.size(); j++)
				vis[Integer.valueOf(result1.get(j)[1])]++;
		}
		int[] ord = new int[1000];
		for (int i = 0; i < 1000; i++)
			ord[i] = i;
		for (int i = 0; i < 1000; i++)
			for (int j = i + 1; j < 1000; j++)
			{
				if (vis[i] < vis[j])
				{
					int tmp = vis[i]; vis[i] = vis[j]; vis[j] = tmp;
					tmp = ord[i]; ord[i] = ord[j]; ord[j] = tmp;
				}
			}
		String output = "";
		for (int i = 0; i < 1000; i++)
		{
			if (vis[i] == 0)
				break;
			if (ord[i] == pid)
				continue;
			output = output + "We suggested the POI " + ord[i] + ", with " + vis[i] + " times of visited.<br>";
		}
		return output;
	}
}
