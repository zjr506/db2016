<%@page import="interfaces.FeedbackManager"%>
<%@ page language="java" import="acmdb.*" contentType="text/html" pageEncoding="UTF-8"%>
<html>
<head>
<title>Two degrees of separation</title>
<script LANGUAGE="javascript">

function check_all_fields(form_obj){
	if (form_obj.u1.value == "" || form_obj.u2.value == ""){
		alert("All the field should be filled!");
		return false;
	}
	return true;
}

</script> 
</head>
<body>

<%
String tag = request.getParameter("tag");
if (tag == null){
	//share all the users!
%>
	Check the degree: <BR>
	<form name="reg" method=get onsubmit="return check_all_fields(this)" action="task12.jsp">
		<input type=hidden name="tag" value="yes">
		User1: <input type=text name="u1" length=20><br>
		User2: <input type=text name="u2" length=20><br>
		<input type=submit>
	</form>
	<BR><BR>
<%
} else {

	String u1 = request.getParameter("u1"), u2 = request.getParameter("u2");
	//
	%>
	The degree is: <%=FeedbackManager.degree(u1, u2) %>
<%
}
if ((int)session.getAttribute("type") == 0){
%>
<BR><a href="user.jsp"> Back </a>
<%
}
else{
%>
<BR><a href="admin.jsp"> Back </a>
<%
}
%>
</body>
