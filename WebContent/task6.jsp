<%@page import="interfaces.FeedbackManager"%>
<%@page import="interfaces.POIManager"%>
<%@page import="java.sql.Date"%>
<%@ page language="java" import="acmdb.*" contentType="text/html" pageEncoding="UTF-8"%>
<html>
<head>
<title>Feedback recordings</title>
<script LANGUAGE="javascript">

function check_all_fields(form_obj){
	if (form_obj.id.value == "" || form_obj.date.value == "" || form_obj.scr.value == ""){
		alert("All the field should be filled!");
		return false;
	}
	return true;
}

</script> 
</head>
<body>

<%
String tag = request.getParameter("tag");
if (tag == null){
	//share all the POIs!
%>
	Give your feedback: <b>(Attention: nochanges are allowed!)</b><BR>
	<form name="reg" method=get onsubmit="return check_all_fields(this)" action="task6.jsp">
		<input type=hidden name="tag" value="yes">
		POI Name: <input type=text name="id" length=20><br>
		Date: <input type=date name="date" length=20><br>
		Score: <input type=number name="scr" length=20><br>
		Optional text(no more 100 characters): <input type=text name="txt" length=100><br>
		<input type=submit>
	</form>
	<BR><BR>
<%
} else {

	String name = request.getParameter("id"), txt = request.getParameter("txt");
	Date date = Date.valueOf(request.getParameter("date")); 
	int scr = Integer.valueOf(request.getParameter("scr")); 
	String login = (String)session.getAttribute("nowuser");
	int pid = POIManager.getPid(name);
	if (pid < 0){ %>
		<script>
			alert("No such POI!");
		</script>
	<%}else{
		int ret = FeedbackManager.addFeekback(login, pid, date, scr, txt);
		if (ret == 0) { %>
			<script>
			alert("You can't feedback one POI twice!");
			</script>
		<%} else {
		%><script>
			alert("Feedback successfully!");
		</script><%
		}
	}
}
if ((int)session.getAttribute("type") == 0){
%>
<BR><a href="user.jsp"> Back </a>
<%
}
else{
%>
<BR><a href="admin.jsp"> Back </a>
<%
}
%>
</body>
