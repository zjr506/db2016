<%@ page language="java" import="acmdb.*" contentType="text/html" pageEncoding="UTF-8"%>
<html>
<head>
<title>Update information</title>
<script LANGUAGE="javascript">

function check_all_fields(form_obj){
	if (form_obj.id.value == "" || form_obj.psw.value == "" || form_obj.cpsw.value == "" ||
		form_obj.name.value == "" || form_obj.addr.value == "" || form_obj.phone.value == ""){
		alert("All the field should be filled!");
		return false;
	}
	if (form_obj.psw.value != form_obj.cpsw.value){
		alert("The passwords are not equal!");
		return false;
	}
	return true;
}

</script> 
</head>
<body>

<%
String tag = request.getParameter("tag");
if (tag == null){
%>
	Enter your new information:<BR>
	<form name="reg" method=get onsubmit="return check_all_fields(this)" action="userinfo.jsp">
		<input type=hidden name="tag" value="yes">
		ID: <input type=text name="id" length=20><BR>
		New Password: <input type=password name="psw" length=20><BR>
		Confirm password: <input type=password name="cpsw" length=20><BR>
		Name: <input type=text name="name" length=20><BR>
		Address: <input type=text name="addr" length=50><BR>
		Phone: <input type=number name="phone" length=20><BR>
		<input type=submit>
	</form>
	<BR><BR>
<%
} else {

	String id = request.getParameter("id"), psw = request.getParameter("psw"), name = request.getParameter("name"),
			addr = request.getParameter("addr"), phone = request.getParameter("phone");
	//update user!
%>
	<script>
		if (!alert("Change successfully."))
		{
			<% if ((int)session.getAttribute("type") == 0){%>
			location.href = 'user.jsp';
			<%}else{%>
			location.href = 'admin.jsp';
			<%}%>
		}
	</script>
<%
}

if ((int)session.getAttribute("type") == 0){
%>
<BR><a href="user.jsp"> Back </a>
<%
}
else{
%>
<BR><a href="admin.jsp"> Back </a>
<%
}
%>
</body>
