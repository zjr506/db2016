package interfaces;

import java.io.FileInputStream;

public class SQLInitializer {

	public SQLInitializer() {
		// TODO Auto-generated constructor stub
	}
	
	public static void initialize(String file) throws Exception {
		try {            
            String path = Thread.currentThread().getContextClassLoader().getResource(file).getPath();
            System.err.println("The path of .sql file is " + path);
            SQLExecutor.runScript(new FileInputStream(path));
        } catch (Exception e) {
        	System.err.println("Initialization failed!");
            throw(e);
        }
	}

}
